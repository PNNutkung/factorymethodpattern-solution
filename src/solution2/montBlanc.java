package solution2;

public class montBlanc implements Dessert {

	@Override
	public String showDetail() {
		return "Mont Blanc";
	}

	@Override
	public String showPrice() {
		return "50 Baht.";
	}

}

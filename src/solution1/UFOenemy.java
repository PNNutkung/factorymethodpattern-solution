package solution1;

public class UFOenemy implements Enemy {

	@Override
	public String attack() {
		return "Hit player 10 damage";
	}

	@Override
	public String showDetail() {
		return "I'm UFO!!";
	}

	@Override
	public String followPlayer() {
		return "Following player";
	}

}
